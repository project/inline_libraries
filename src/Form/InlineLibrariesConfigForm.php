<?php

namespace Drupal\inline_libraries\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Configure inline libraries settings.
 *
 * @internal
 */
class InlineLibrariesConfigForm extends ConfigFormBase {

  /**
     * Config settings.
     *
     * @var string
     */
  const SETTINGS = 'inline_libraries.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'inline_libraries_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = ($this->config(static::SETTINGS)->get('data')) ? $this->config(static::SETTINGS)->get('data') : [];
    $librariesInfo = \Drupal::service('inline_libraries.default')->getAllLibraries();

    $form = [
      '#attributes' => [
        'class' => ['system-modules'],
      ],
    ];

    $form['filters'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['table-filter', 'js-show'],
      ],
    ];

    $form['filters']['text'] = [
      '#type' => 'search',
      '#title' => $this->t('Filter Libraries'),
      '#title_display' => 'invisible',
      '#size' => 30,
      '#placeholder' => $this->t('Filter by name.'),
      '#description' => $this->t('Enter a part of the library name.'),
      '#attributes' => [
        'class' => ['table-filter-text'],
        'data-table' => '#inline-libraries-form',
        'autocomplete' => 'off',
      ],
    ];

    // Iterate over each of the libraries.
    $form['libraries']['#tree'] = TRUE;

    $output = [];
    foreach ($librariesInfo as $package => $library) {
      foreach ($library as $library_name => $library_details) {
        if (!empty($library_details['js']) || !empty($library_details['css'])) {
          $form['libraries'][$package][$library_name] = $this->buildRow($library_name, $library_details, $config);
        }
      }
    }

    foreach (Element::children($form['libraries']) as $package) {
      $form['libraries'][$package] += [
        '#type' => 'details',
        '#title' => str_replace('_', ' ', $package),
        '#open' => TRUE,
        '#theme' => 'inline_libraries_form',
        '#attributes' => ['class' => ['package-listing']],
      ];
    }

    $form['#attached']['library'][] = 'core/drupal.tableresponsive';
    $form['#attached']['library'][] = 'system/drupal.system.modules';

    return parent::buildForm($form, $form_state);
  }

  /**
   * Builds a table row for the inline libraries settings page.
   *
   * @param string $library_name
   *   Library name.
   * @param array $library_details
   *   Library detail array.
   * @param array $config
   *   Configuration from the inline library.
   *
   * @return array
   *   The form row for the given module.
   */
  protected function buildRow($library_name, array $library_details, array $config) {
    $row['name']['#markup'] = $library_name;

    foreach ($library_details['css'] as $css_library) {
      if ($css_library['type'] == 'file') {
        $paths[] = $css_library['data'];
      }
    }

    foreach ($library_details['js'] as $js_library) {
      if ($js_library['type'] == 'file') {
        $paths[] = $js_library['data'];
      }
    }

    $row['path'] = [
      '#theme' => 'item_list',
      '#items' => $paths,
    ];

    // Present a checkbox for installing and indicating the status of a module.
    $row['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Install'),
    ];

    $flatten_config = [];
    foreach ($config as $a) {
      $flatten_config = array_merge($flatten_config, $a);
    }
    if (in_array($library_name, $flatten_config)) {
      $row['enable']['#default_value'] = TRUE;
    }

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $inline_libs = [];
    $libraries = $form_state->getValue(['libraries'], FALSE);
    foreach ($libraries as $key => $library) {
      foreach ($library as $library_name => $status) {
        if ($status['enable']) {
          $inline_libs[$key][] = $library_name;
        }
      }
    }
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('data', $inline_libs)
      ->save();

    parent::submitForm($form, $form_state);
  }

}

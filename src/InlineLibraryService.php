<?php

namespace Drupal\inline_libraries;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;

/**
 * Class InlineLibraryService.
 *
 * @package Drupal\inline_libraries
 */
class InlineLibraryService {
  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Theme Handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The Library Discovery service.
   *
   * @var \Drupal\Core\Asset\LibraryDiscovery
   */
  protected $libraryDiscovery;

  /**
   * Constructs a new Library Parser.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The Module Handler service.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The Theme Handler service.
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   The Library Discovery Collector service.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    ThemeHandlerInterface $themeHandler,
    LibraryDiscoveryInterface $libraryDiscovery
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->themeHandler = $themeHandler;
    $this->libraryDiscovery = $libraryDiscovery;
  }

  /**
   * Fetch all the libraries available.
   */
  public function getAllLibraries() {
    $extensions = array_merge(
      array_keys($this->moduleHandler->getModuleList()),
      array_keys($this->themeHandler->listInfo())
    );
    foreach ($extensions as $extension_name) {
      $libraries[$extension_name] = $this->libraryDiscovery->getLibrariesByExtension($extension_name);
    }
    return $libraries;
  }

  /**
   * Get details for the inline libraries.
   */
  public function getIndividualLib($inline_libraries) {
    $root = \Drupal::root();
    $inline_files = [];
    $allowed_types = ['css', 'js'];

    foreach ($inline_libraries as $library_name => $extension_name) {
      $library = $this->libraryDiscovery->getLibrariesByExtension($library_name);
      $i = 0;
      foreach ($extension_name as $name) {
        $lib = $library[$name];

        foreach ($allowed_types as $type) {
          if (!empty($lib[$type])) {
            foreach ($lib[$type] as $data) {
              if ($data['type'] == 'file') {
                $inline_files[] = [
                  'path' => $root . '/' . $data['data'],
                  'extension' => $library_name . '_' . $name . '_' . $i,
                  'type' => $type,
                ];
                $i++;
              }
            }
          }
        }
      }
    }
    return $inline_files;
  }

  /**
   * Unset the files of the inline libraries.
   */
  public function getUnsetLibraryFiles($inline_libraries, $type) {
    $unset_libraries = [];

    foreach ($inline_libraries as $library_name => $extension_name) {
      $library = $this->libraryDiscovery->getLibrariesByExtension($library_name);
      foreach ($extension_name as $name) {
        $lib = $library[$name];
        if (!empty($lib[$type])) {
          foreach ($lib[$type] as $data) {
            if ($data['type'] == 'file') {
              $unset_libraries[] = $data['data'];
            }
          }
        }
      }
    }
    return $unset_libraries;
  }

}

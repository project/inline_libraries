<?php

/**
 * @file
 * Admin page callbacks for the inline libraries.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element;
use Drupal\Core\Template\Attribute;

/**
 * Prepares variables for the library details templates.
 *
 * Default template: system-modules-details.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - form: A render element representing the form. The main form element
 *     represents a package, and child elements of the form are individual
 *     projects. Each project (library) is an associative array containing the
 *     following elements:
 *     - name: The name of the library.
 *     - enable: A checkbox for enabling the library.
 *     - description: A description of the library.
 *     - #attributes: A list of attributes for the library wrapper.
 *
 * @see \Drupal\system\Form\InlineLibrariesConfigForm
 */
function template_preprocess_inline_libraries_form(array &$variables) {
  $form = $variables['form'];

  $variables['libraries'] = [];
  // Iterate through all the libraries, which are children of this element.
  foreach (Element::children($form) as $key) {
    // Stick the key into $library for easier access.
    $library = $form[$key];

    unset($library['enable']['#title']);
    // Add the checkbox to allow installing new libraries and to show the
    // installation status of the module.
    $library['checkbox'] = $library['enable'];

    // Add the library label and expand/collapse functionality.
    $id = Html::getUniqueId('library-' . $key);
    $library['id'] = $id;
    $library['enable_id'] = $library['enable']['#id'];

    // @todo Remove early rendering and use safe_join in the Twig template once
    //   https://www.drupal.org/node/2579091 is fixed.
    $renderer = \Drupal::service('renderer');
    $machine_name_render = [
      '#prefix' => '<span dir="ltr" class="table-filter-text-source">',
      '#plain_text' => $key,
      '#suffix' => '</span>',
    ];
    $library['machine_name'] = $renderer->render($machine_name_render);

    $library['attributes'] = new Attribute($library['#attributes']);
    $variables['libraries'][] = $library;
  }
}
